package io.finer.ads.jeecg.mapper;

import io.finer.ads.jeecg.entity.ClickLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 点击日志
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
public interface ClickLogMapper extends BaseMapper<ClickLog> {

}
