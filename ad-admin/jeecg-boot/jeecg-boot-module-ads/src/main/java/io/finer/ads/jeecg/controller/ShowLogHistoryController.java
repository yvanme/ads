package io.finer.ads.jeecg.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import io.finer.ads.jeecg.entity.ShowLogHistory;
import io.finer.ads.jeecg.service.IShowLogHistoryService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 呈现日志-历史
 * @Author: jeecg-boot
 * @Date:   2020-06-16
 * @Version: V1.0
 */
@Api(tags="呈现日志-历史")
@RestController
@RequestMapping("/showLogHistory")
@Slf4j
public class ShowLogHistoryController extends JeecgController<ShowLogHistory, IShowLogHistoryService> {
	@Autowired
	private IShowLogHistoryService showLogHistoryService;

	/**
	 * 分页列表查询
	 *
	 * @param showLogHistory
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "呈现日志-历史-分页列表查询")
	@ApiOperation(value="呈现日志-历史-分页列表查询", notes="呈现日志-历史-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ShowLogHistory showLogHistory,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<ShowLogHistory> queryWrapper = QueryGenerator.initQueryWrapper(showLogHistory, req.getParameterMap());
		Page<ShowLogHistory> page = new Page<ShowLogHistory>(pageNo, pageSize);
		IPage<ShowLogHistory> pageList = showLogHistoryService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param showLogHistory
	 * @return
	 */
	@AutoLog(value = "呈现日志-历史-添加")
	@ApiOperation(value="呈现日志-历史-添加", notes="呈现日志-历史-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ShowLogHistory showLogHistory) {
		showLogHistoryService.save(showLogHistory);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param showLogHistory
	 * @return
	 */
	@AutoLog(value = "呈现日志-历史-编辑")
	@ApiOperation(value="呈现日志-历史-编辑", notes="呈现日志-历史-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ShowLogHistory showLogHistory) {
		showLogHistoryService.updateById(showLogHistory);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "呈现日志-历史-通过id删除")
	@ApiOperation(value="呈现日志-历史-通过id删除", notes="呈现日志-历史-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		showLogHistoryService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "呈现日志-历史-批量删除")
	@ApiOperation(value="呈现日志-历史-批量删除", notes="呈现日志-历史-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.showLogHistoryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "呈现日志-历史-通过id查询")
	@ApiOperation(value="呈现日志-历史-通过id查询", notes="呈现日志-历史-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ShowLogHistory showLogHistory = showLogHistoryService.getById(id);
		if(showLogHistory==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(showLogHistory);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param showLogHistory
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ShowLogHistory showLogHistory) {
        return super.exportXls(request, showLogHistory, ShowLogHistory.class, "呈现日志-历史");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ShowLogHistory.class);
    }

}
